Project Git Instructions
------------------------

This module adds the "Verion control" tab to certian pages on Drupal.org. This includes modules, themes and installation profiles.

This modules is specifically build for use on drupal.org.

If you are hosting Git repositories yourself and make use of the versioncontrol API modules in drupal then maybe this module can be of service. It has however not been extensively tested on other installations.

See: http://drupal.org/project/project_git_instructions
